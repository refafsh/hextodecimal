﻿using System.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexToDecimal
{
    class Program
    {
        private static string HelpText = "\n\nHexToDecimal helptext:\n" +
                                         "\n------------------------------------\n" +
                                         "\nRequired arguments:\n" +
                                         "   Hex=[hexnumber]           Hex-number to transform\n" +
                                         "\n------------------------------------\n" +
                                         "\nOptional arguments:\n" +
                                         "   Outfile=[filename]        File to output result to - if not set, output is send to prompt\n" +
                                         "   PreCut=[number]           Number of ciffers to remove from beginning of decimal number\n" +
                                         "   PrePend=[string]          String to set in front of the card number e.g. \"A\"=>A613612345\n" +
                                         "   \n" +
                                         "   \n";
        private static bool ShowHelp = false;
        private static string Outfile = "";
        private static string Input = "";
        private static string PrePend = "";
        private static int PreCut = 0;
        private static bool Debug = false;

        static void Main(string[] args)
        {
            if (args.Count() < 1)
            {
                PrintHelpText();
                Environment.Exit(0);
            }
            else
            {
                getArguments(args);
                try
                {
                    Outfile.Replace("\\", "\\\\");

                    Int64 decimalValue = Int64.Parse(Input, System.Globalization.NumberStyles.HexNumber);
                    string retur = decimalValue.ToString();

                    if (PreCut > 0)
                        retur = retur.Substring(PreCut);
                    if (PrePend != "")
                        retur = PrePend + retur;
                    if (Outfile.Length > 0)
                        System.IO.File.WriteAllText(Outfile, retur);

                    Console.WriteLine(retur);

                }
                catch (Exception e)
                {
                    if (Debug)
                    {
                        Console.WriteLine(args.Count().ToString());
                        Console.WriteLine("Der opstod en fejl:\n" + "\n" + args[0] + "\n" + args[1] + "\n" + e.Message);
                        Thread.Sleep(20);
                    }

                    PrintHelpText();

                }
            }
        }

        private static void PrintHelpText()
        {
            Console.WriteLine(HelpText);
        }
        private static void getArguments(string[] args)
        {
            foreach (string arg in args)
            {
                string a = "";
                if (arg.IndexOf("=") > 0)
                    a = arg.Substring(0, arg.IndexOf("="));
                string b = arg.Substring(arg.IndexOf("=") + 1);

                a = a.ToLower();

                switch (a)
                {
                    case "hex":
                        Input = b;
                        break;
                    case "outfile":
                        Outfile = b;
                        break;
                    case "prepend":
                        PrePend = b;
                        break;
                    case "precut":
                        try
                        {
                            PreCut = int.Parse(b);
                        }
                        catch
                        {
                            PreCut = 0;
                        }
                        break;
                    case "debug":
                        Debug = true;
                        break;
                    default:
                        break;

                }
            }
        }
    }
}
